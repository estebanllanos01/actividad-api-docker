FROM openjdk:17

VOLUME /tmp

EXPOSE 8080

COPY target/Supermercado-0.0.1-SNAPSHOT.jar Supermercado.jar

ENTRYPOINT ["java", "-jar", "Supermercado.jar"]

#docker build -t supermercado-api .
#docker run -p 8080:8080 -d supermercado-api