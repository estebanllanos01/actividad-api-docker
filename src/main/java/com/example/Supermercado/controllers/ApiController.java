package com.example.Supermercado.controllers;

import com.example.Supermercado.dao.Datos;
import com.example.Supermercado.models.Cajero;
import com.example.Supermercado.models.Merma;
import com.example.Supermercado.models.Producto;
import com.example.Supermercado.models.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("supermercado")
public class ApiController {
    private static Datos datos = new Datos();

    @GetMapping("/productos")
    public ArrayList<Producto> obtenerProductos(){
        return datos.getProductos();
    }

    @GetMapping("/mermas")
    public ArrayList<Merma> obtenerMermas(){
        return datos.getMermas();
    }

    @GetMapping("/cajeros")
    public ArrayList<Cajero> obtenerCajeros(){
        return datos.getCajeros();
    }

    @GetMapping("reponedores")
    public ArrayList<Reponedor> obtenerReponedores(){
        return datos.getReponedores();
    }
}
