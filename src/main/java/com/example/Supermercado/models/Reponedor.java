package com.example.Supermercado.models;

public class Reponedor extends Usuario{
    private String seccion;

    public Reponedor(String nombre, String rut, String usuario, String contraseña, String seccion) {
        super(nombre, rut, usuario, contraseña);
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
