package com.example.Supermercado.dao;

import com.example.Supermercado.models.Cajero;
import com.example.Supermercado.models.Merma;
import com.example.Supermercado.models.Producto;
import com.example.Supermercado.models.Reponedor;

import java.util.ArrayList;
import java.util.Arrays;

public class Datos {

    private ArrayList<Cajero> cajeros;

    private ArrayList<Reponedor> reponedores;

    private ArrayList<Producto> productos;

    private ArrayList<Merma> mermas;

    public Datos() {

        Cajero cajero1 = new Cajero("Esteban","11111111-1","esteban01","123456",35);
        Cajero cajero2 = new Cajero("Andres","22222222-2","andres01","123456",20);
        Cajero cajero3 = new Cajero("Pablo","33333333-3","pablo01","123456",18);
        Cajero cajero4 = new Cajero("Jose","44444444-4","jose01","123456",41);
        Cajero cajero5 = new Cajero("Marta","55555555-5","marta01","123456",29);

        cajeros = new ArrayList<>(Arrays.asList(cajero1,cajero2,cajero3,cajero4,cajero5));

        Reponedor reponedor1 = new Reponedor("Miguel","66666666-6","miguel01","123456","Bebestible");
        Reponedor reponedor2 = new Reponedor("Luis","77777777-7","luis01","123456","Lacteos");
        Reponedor reponedor3 = new Reponedor("Josefa","88888888-8","josefa01","123456","Carniceria");

        reponedores = new ArrayList<>(Arrays.asList(reponedor1,reponedor2,reponedor3));

        Producto producto1 = new Producto("Coca Cola", "Bebestible", 100, 1200, "Pasillo 1");
        Producto producto2 = new Producto("Vino", "Bebestible", 50, 6000, "Pasillo 1");
        Producto producto3 = new Producto("Leche", "Lacteos", 40, 800, "Pasillo 2");
        Producto producto4 = new Producto("Queso", "Lacteos", 25, 1600, "Pasillo 2");
        Producto producto5 = new Producto("Aceite", "Abarrotes", 61, 2800, "Pasillo 3");
        Producto producto6 = new Producto("Arroz", "Abarrotes", 20, 900, "Pasillo 3");
        Producto producto7 = new Producto("Pechuga Pollo", "Carniceria", 50, 6000, "Pasillo 4");
        Producto producto8 = new Producto("Lomo Liso", "Carniceria", 50, 6000, "Pasillo 4");

        productos = new ArrayList<>(Arrays.asList(producto1, producto2, producto3, producto4, producto5, producto6, producto7, producto8));

        Merma merma1 = new Merma("Producto Vencido","Pechuga de Pollo Vencida",10);
        Merma merma2 = new Merma("Producto Roto","Botella de Vino Rota",5);
        Merma merma3 = new Merma("Producto Robado","Botella de Coca Cola robada",2);

        mermas = new ArrayList<>(Arrays.asList(merma1,merma2,merma3));

    }

    public ArrayList<Cajero> getCajeros() {
        return cajeros;
    }

    public ArrayList<Reponedor> getReponedores() {
        return reponedores;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public ArrayList<Merma> getMermas() {
        return mermas;
    }
}
